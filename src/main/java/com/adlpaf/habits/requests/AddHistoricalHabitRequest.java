package com.adlpaf.habits.requests;

public class AddHistoricalHabitRequest {
    private Long id;
    
    public AddHistoricalHabitRequest(Long id) {
        this.id = id;
    }

    public AddHistoricalHabitRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
