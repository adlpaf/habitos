package com.adlpaf.habits.service;

import com.adlpaf.habits.models.HistoricalHabits;

public interface IHistoricalHabitService {

    public HistoricalHabits save(HistoricalHabits personalHabits);

}
