package com.adlpaf.habits.service;

import java.util.Optional;

import com.adlpaf.habits.models.User;

public interface IUserService {

    public Optional<User> findOneByEmail(String email);

    public User save(User user);

}
