package com.adlpaf.habits.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adlpaf.habits.models.HistoricalHabits;
import com.adlpaf.habits.repository.HistoricalHabitsRepository;

@Service
public class HistoricalHabitsServiceImpl implements IHistoricalHabitService {

    @Autowired
    HistoricalHabitsRepository historicalHabitsRepository;

    @Override
    public HistoricalHabits save(HistoricalHabits historicalHabits) {
        return historicalHabitsRepository.save(historicalHabits);
    }


}
