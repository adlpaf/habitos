package com.adlpaf.habits.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adlpaf.habits.models.PersonalHabits;
import com.adlpaf.habits.repository.PersonalHabitRepository;
import com.adlpaf.habits.responses.ListPersonalHabitsForToday;

@Service
public class PersonalHabitServiceImpl implements IPersonalHabitService {

    @Autowired
    PersonalHabitRepository personalHabitRepository;

    @Override
    public void delete(Long id) {
        personalHabitRepository.deleteById(id);
    }

    @Override
    public List<PersonalHabits> getAll() {
        return personalHabitRepository.findAll();
    }

    @Override
    public PersonalHabits getById(Long id) {
        return personalHabitRepository.findById(id).orElse(null);
    }

    @Override
    public PersonalHabits save(PersonalHabits personalHabits) {
        return personalHabitRepository.save(personalHabits);
    }

    @Override
    public List<PersonalHabits> getActive() {
        return personalHabitRepository.findByDeletedAt(null);
    }

    @Override
    public List<ListPersonalHabitsForToday> getByUserId(Long user_id) {
        return personalHabitRepository.findByUserIdAndDeletedAt(user_id);
    }

}
