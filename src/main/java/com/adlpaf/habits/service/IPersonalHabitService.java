package com.adlpaf.habits.service;

import java.util.List;

import com.adlpaf.habits.models.PersonalHabits;
import com.adlpaf.habits.responses.ListPersonalHabitsForToday;

public interface IPersonalHabitService {

    public List<PersonalHabits> getAll();

    public PersonalHabits save(PersonalHabits personalHabits);

    public PersonalHabits getById(Long id);

    public void delete(Long id);

    public List<PersonalHabits> getActive();

    public List<ListPersonalHabitsForToday> getByUserId(Long user_id);
}
