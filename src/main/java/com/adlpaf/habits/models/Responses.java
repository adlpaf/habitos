package com.adlpaf.habits.models;

public class Responses {
    private User user;
    private String token;
    
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public Responses(User user, String token) {
        this.user = user;
        this.token = token;
    }
}
