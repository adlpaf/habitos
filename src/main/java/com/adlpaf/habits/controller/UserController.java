package com.adlpaf.habits.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.Authentication;

import com.adlpaf.habits.models.Responses;
import com.adlpaf.habits.models.User;
import com.adlpaf.habits.repository.UserRepository;
import com.adlpaf.habits.requests.LoginRequest;
import com.adlpaf.habits.security.TokenUtils;
import com.adlpaf.habits.service.UserServiceImpl;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

// import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RestController
@RequestMapping("/api/")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JavaMailSender mail;

    private TokenUtils tokenUtils;
    
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("register")
    public ResponseEntity<User> register(
        @RequestParam String name, 
        @RequestParam String email, 
        @RequestParam String password, 
        @RequestParam String passwordConfirmed
    ) {
        if (!password.equals(passwordConfirmed)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        
        User searchUser = userRepository.findOneByEmail(email).orElse(null);
        if (searchUser != null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        String hashedPassword = passwordEncoder.encode(password);

        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(hashedPassword);

        User newUser = userServiceImpl.save(user);

        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PostMapping("forgotPassword")
    public ResponseEntity<User> forgotPassword(@RequestParam String email) {
        User user = userRepository.findOneByEmail(email).orElse(null);

        if (user == null) {
            return new ResponseEntity<>(user, HttpStatus.BAD_REQUEST);
        }

        System.out.println(user);
        System.out.println(email);
        //TODO: send email URL to user
        SimpleMailMessage emailMessage = new SimpleMailMessage();
        emailMessage.setTo(email);
        emailMessage.setFrom("adlpaf.dev@gmail.com");
        emailMessage.setSubject("Forgot password");
        emailMessage.setText("Click on the next link\n<a href='http://localhost:4200/'>Reset the password</a>");

        mail.send(emailMessage);
        
        return new ResponseEntity<>(user, HttpStatus.ACCEPTED);
    }
    
    @PostMapping("resetPassword")
    public ResponseEntity<User> resetPassword(@RequestParam String email, @RequestParam String password, @RequestParam String confirmedPassword) {
        User user = userRepository.findOneByEmail(email).orElse(null);

        if ((user == null) || (!password.equals(passwordEncoder))) {
            return new ResponseEntity<>(user, HttpStatus.BAD_REQUEST);
        }

        user.setPassword(passwordEncoder.encode(password));

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PostMapping("login")
    public ResponseEntity<Responses> login(@RequestBody LoginRequest body) {

        try {
            String email = body.getCorreo();
            String password = body.getPassword();
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            String emailAuth = authentication.getName();
            
            User user = new User();
            user.setEmail(emailAuth);
            User userLogin = userRepository.findOneByEmail(email).orElse(null);
            if (userLogin == null) {
                throw new UsernameNotFoundException("Username not found");
            }
            String token = tokenUtils.createToken(userLogin.getName(), userLogin.getEmail());

            Responses resp = new Responses(userLogin, token);
            
            return new ResponseEntity<>(resp, HttpStatus.OK);
            
        }catch (BadCredentialsException e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }
    }

    /* @PostMapping("logout")
    public ResponseEntity<Responses> logout(@RequestBody LoginRequest body) {

        try {
            String email = body.getCorreo();
            String password = body.getPassword();
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            System.out.println("TESTING");
            String emailAuth = authentication.getName();
            User user = new User();
            user.setEmail(emailAuth);
            User userLogin = userRepository.findOneByEmail(email).orElse(null);
            if (userLogin == null) {
                throw new UsernameNotFoundException("Username not found");
            }
            String token = tokenUtils.createToken(userLogin.getName(), userLogin.getEmail());

            Responses resp = new Responses(userLogin, token);
            
            return new ResponseEntity<>(null, HttpStatus.OK);
            
        }catch (BadCredentialsException e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }
    } */

    @GetMapping("check-status")
    public ResponseEntity<Responses> check_status(HttpServletRequest request) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String token = request.getHeader("Authorization");
            if (token != null && token.startsWith("Bearer ")) {
                token = token.replace("Bearer ", "");
                UsernamePasswordAuthenticationToken usernamePAT = TokenUtils.getAuthentication(token);
                SecurityContextHolder.getContext().setAuthentication(usernamePAT);
            }
    
            String username = authentication.getName();
            User user = userRepository.findOneByEmail(username).orElse(null);

            Responses resp = new Responses(user, token);
            
            return new ResponseEntity<>(resp, HttpStatus.OK);
            
        }catch (BadCredentialsException e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }
    }
}
