package com.adlpaf.habits.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// import com.adlpaf.habits.models.HistoricalHabits;
import com.adlpaf.habits.models.PersonalHabits;
import com.adlpaf.habits.models.User;
import com.adlpaf.habits.repository.UserRepository;
import com.adlpaf.habits.responses.ListPersonalHabitsForToday;
import com.adlpaf.habits.service.PersonalHabitServiceImpl;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("/api/v1")
public class PersonalHabitsController {

    @Autowired
    PersonalHabitServiceImpl personalHabitServiceImpl;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/personalHabits")
    public List<ListPersonalHabitsForToday> getPersonalHabits() {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        User user = userRepository.findOneByEmail(email).orElse(null);

        return (List<ListPersonalHabitsForToday>) personalHabitServiceImpl.getByUserId(user.getId());
    }

    @PostMapping("/newHabit")
    public ResponseEntity<PersonalHabits> savePersonalHabit(@RequestParam String name, @RequestParam String description) {
        PersonalHabits personalHabits = new PersonalHabits();
        personalHabits.setCreatedAt();
        personalHabits.setUpdatedAt();
        personalHabits.setName(name);
        personalHabits.setDescription(description);
        PersonalHabits newPersonalHabits = personalHabitServiceImpl.save(personalHabits);
        return new ResponseEntity<>(newPersonalHabits, HttpStatus.CREATED);
    }

    @GetMapping("/personalHabit/{id}")
    public ResponseEntity<PersonalHabits> getPersonalHabitsId(@PathVariable Long id) {
        PersonalHabits personalHabits = personalHabitServiceImpl.getById(id);
        return ResponseEntity.ok(personalHabits);
    }

    @PutMapping("/personalHabit/{id}")
    public ResponseEntity<PersonalHabits> update(@PathVariable Long id, @RequestBody PersonalHabits personalHabits) {
        PersonalHabits personalHabitsById = personalHabitServiceImpl.getById(id);
        personalHabitsById.setName(personalHabits.getName());
        personalHabitsById.setDescription(personalHabits.getDescription());

        PersonalHabits personalHabitsUpdated = personalHabitServiceImpl.save(personalHabitsById);

        return new ResponseEntity<>(personalHabitsUpdated, HttpStatus.CREATED);
    }

    @DeleteMapping("/personalHabit/{id}")
    public ResponseEntity<HashMap<String, Boolean>> deletePersonalHabits(@PathVariable Long id) {
        PersonalHabits personalHabitsById = personalHabitServiceImpl.getById(id);
        personalHabitsById.setDeletedAt();

        personalHabitServiceImpl.save(personalHabitsById);

        HashMap<String, Boolean> personalHabitStatus = new HashMap<>();
        personalHabitStatus.put("Deleted", true);

        return ResponseEntity.ok(personalHabitStatus);
    }

}
