package com.adlpaf.habits.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
// import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adlpaf.habits.models.HistoricalHabits;
// import com.adlpaf.habits.models.Responses;
import com.adlpaf.habits.models.User;
import com.adlpaf.habits.repository.UserRepository;
import com.adlpaf.habits.requests.AddHistoricalHabitRequest;
// import com.adlpaf.habits.requests.LoginRequest;
import com.adlpaf.habits.security.TokenUtils;
import com.adlpaf.habits.service.HistoricalHabitsServiceImpl;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1")
public class HistoricalHabitsController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HistoricalHabitsServiceImpl historicalHabitsServiceImpl;

    @PostMapping("/addHistoricalHabit")
    public ResponseEntity<HistoricalHabits> addHistoricalHabit(@RequestBody AddHistoricalHabitRequest body, HttpServletRequest request) {

        try {
            Long historical_habits_id = body.getId();

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String token = request.getHeader("Authorization");
            if (token != null && token.startsWith("Bearer ")) {
                token = token.replace("Bearer ", "");
                UsernamePasswordAuthenticationToken usernamePAT = TokenUtils.getAuthentication(token);
                SecurityContextHolder.getContext().setAuthentication(usernamePAT);
            }
    
            String username = authentication.getName();
            User user = userRepository.findOneByEmail(username).orElse(null);

            HistoricalHabits historicalHabits = new HistoricalHabits();
            historicalHabits.setUser_id(user.getId());
            historicalHabits.setPersonal_habit_id(historical_habits_id);

            HistoricalHabits newHistoricalHabits = historicalHabitsServiceImpl.save(historicalHabits);

            return new ResponseEntity<>(newHistoricalHabits, HttpStatus.OK);
            
        }catch (BadCredentialsException e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); //.body(errorResponse);
        }
}


}
