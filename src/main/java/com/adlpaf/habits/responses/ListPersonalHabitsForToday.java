package com.adlpaf.habits.responses;

import java.sql.Timestamp;
// import java.util.List;

// import com.adlpaf.habits.models.PersonalHabits;
// import com.adlpaf.habits.models.User;

public interface ListPersonalHabitsForToday {

    Long getId();
    Long getUserId();
    Long getPersonalHabitsId();

    String getName();
    String getDescription();

    Timestamp getCreatedAt();
    Timestamp getUpdatedAt();
    Timestamp getDeletedAt();

    String getChecked();

    // List<User> user;
    // List<PersonalHabits> personalHabits;

}
