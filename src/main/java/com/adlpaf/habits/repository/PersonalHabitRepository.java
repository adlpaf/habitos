package com.adlpaf.habits.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
// import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adlpaf.habits.models.PersonalHabits;
import com.adlpaf.habits.responses.ListPersonalHabitsForToday;

@Repository
public interface PersonalHabitRepository extends JpaRepository<PersonalHabits, Long> {

    public List<PersonalHabits> findByDeletedAt(Timestamp deletedAt);
    // public List<PersonalHabits> findByUserId(Long user_id);

    @Query(value = "select ph.id as id, ph.user_id as userId, ph.name as name, ph.description as description, " +
        "ph.created_at as createdAt, ph.updated_at as updatedAt, ph.deleted_at as deletedAt, (CASE when hh.personal_habit_id IS NOT NULL THEN 'true' ELSE 'false' END) as checked " +
        "from personal_habits as ph " +
        "left join historical_habits as hh on hh.personal_habit_id = ph.id and DATE(hh.created_at) = CURDATE() " +
        "WHERE ph.deleted_at IS NULL AND ph.user_id = ?1 GROUP BY id", nativeQuery = true)
    public List<ListPersonalHabitsForToday> findByUserIdAndDeletedAt(Long user_id);

}
