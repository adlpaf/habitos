package com.adlpaf.habits.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adlpaf.habits.models.HistoricalHabits;

public interface HistoricalHabitsRepository  extends JpaRepository<HistoricalHabits, Long> {

}
