FROM openjdk:17
VOLUME /tmp
EXPOSE 8888
ADD ./target/[NOMBRE DEL JAR GENERADO] config-server.jar
ENTRYPOINT ["java", "-jar", "/config-server.jar"]

# Para generar la imagen de Docker
# docker-build -t config-server:v1 .

# Para crear una red en docker
# docker network create springcloud

# Para correr la imagen de Docker
# docker run -p 8888:8888 --name config-server --network springcloud config-server:v1